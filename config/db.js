const mongoose = require("mongoose");

const connectDB = async () => {
  let MONGO_DB_LINK = "";
  try {
    if (process.env.NODE_ENV == "TEST") {
      MONGO_DB_LINK = process.env.MONGO_URI_TEST;
    } else if (process.env.NODE_ENV == "DEVELOPMENT") {
      MONGO_DB_LINK = process.env.MONGO_URI_DEVELOPMENT;
    } else if (process.env.NODE_ENV == "LIVE") {
      MONGO_DB_LINK = process.env.MONGO_URI_LIVE;
    }

    const conn = await mongoose.connect(MONGO_DB_LINK, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log(
      `MongoDB Connected: ${conn.connection.host} in ${process.env.NODE_ENV} mode`
    );
  } catch (error) {
    console.error("MongoDB connection error: ", error);
    process.exit(1);
  }
};

module.exports = connectDB;
