const express = require("express");
const router = express.Router();
const User = require("../models/User");
/**
 * @swagger
 * definitions:
 *   User:
 *     properties:
 *       _id:
 *         type: string
 *       firstname:
 *         type: string
 *       lastname:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * /api/v1/users:
 *   get:
 *     tags:
 *       - Users
 *     description: Returns list of all users
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: List of all the users
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Unable to get all user
 *       400:
 *         description: Unable to get all user
 */
router.get("/", async (req, res) => {
  try {
    const usersData = await User.find({});
    res
      .status(200)
      .send({ success: true, message: "List of all users", users: usersData });
  } catch (error) {
    res.status(400).send({
      message: "Unable to process request to find all users",
      err: err,
    });
  }
});

/**
 * @swagger
 * /api/v1/users:
 *   post:
 *     tags:
 *       - Users
 *     description: To create user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: List of all the users
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Unable to add user
 *       400:
 *         description: Unable to add user
 */
router.post("/", async (req, res) => {
  try {
    const userData = new User(req.body);
    userData
      .save()
      .then((item) => {
        res.status(200).send({
          success: true,
          message: "User created successfully",
          users: [item],
        });
      })
      .catch((err) => {
        res
          .status(404)
          .send({ message: "Unable to save user to storage", err: err });
      });
  } catch (error) {
    res.status(400).send({
      success: false,
      message: `Unable to process add user`,
      err: err,
    });
  }
});

/**
 * @swagger
 * /api/v1/users/:id:
 *   get:
 *     tags:
 *       - Users
 *     description: To get user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return one user based on id
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Unable to identify user
 *       400:
 *         description: Unable to identify user
 */
router.get("/:id", async (req, res) => {
  try {
    await User.findById({ _id: req.params.id })
      .then((user) => {
        if (user) {
          res.status(200).send({
            success: true,
            message: `User is found with id ${req.params.id}`,
            users: user,
          });
        } else {
          res.status(400).send({
            success: false,
            message: `Unable to find User with id ${req.params.id}`,
            docs: docs,
          });
        }
      })
      .catch((err) => {
        if (err) {
          res.status(404).send({
            success: false,
            message: `Unable to procss get User request with id ${req.params.id}`,
            err: err,
          });
        }
      });
  } catch (error) {
    res.status(400).send({
      success: false,
      message: `Unable to procss delete User request with id ${req.params.id}`,
      err: err,
    });
  }
});

/**
 * @swagger
 * /api/v1/users:
 *   delete:
 *     tags:
 *       - Users
 *     description: Delete User based on given id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: User's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Delete's the user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Unable to find user by the given id
 */
router.delete("/:id", async (req, res) => {
  try {
    await User.findOneAndDelete({ _id: req.params.id })
      .then((user) => {
        if (user) {
          res.status(200).send({
            success: true,
            message: `User is deleted with id ${req.params.id}`,
          });
        } else {
          res.status(400).send({
            success: false,
            message: `Unable to find User with id ${req.params.id}`,
            docs: docs,
          });
        }
      })
      .catch((err) => {
        if (err) {
          res.status(404).send({
            success: false,
            message: `Unable to procss delete User request with id ${req.params.id}`,
            err: err,
          });
        }
      });
  } catch (error) {
    res.status(400).send({
      success: false,
      message: `Unable to procss delete User request with id ${req.params.id}`,
      err: err,
    });
  }
});

/**
 * @swagger
 * /api/v1/users:
 *   put:
 *     tags:
 *       - Users
 *     description: Update User based on given id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: User's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Updated's the user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Unable to find user by the given id
 */
router.put("/:id", async (req, res) => {
  try {
    await User.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      { upsert: true },
      function (err, doc) {
        if (err) {
          return res.status(404).send({
            success: false,
            message: `Unable to procss update User request with id ${req.params.id}`,
            err: err,
          });
        } else {
          return res.status(200).send({
            success: true,
            message: `User is updated with id ${req.params.id}`,
          });
        }
      }
    );
  } catch (error) {
    res.status(400).send({
      success: false,
      message: `Unable to procss update User request with id ${req.params.id}`,
      err: err,
    });
  }
});

module.exports = router;
