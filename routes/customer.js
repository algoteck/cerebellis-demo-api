const express = require("express");
const router = express.Router();

/**
 * @swagger
 * /api/v1/customers:
 *  get:
 *    tags:
 *       - Customers
 *    description: Use to request all customers
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/", (req, res) => {
  res.status(200).send("Customer results");
});

/**
 * @swagger
 * /api/v1/customers:
 *    put:
 *      tags:
 *       - Customers
 *      description: Use to return all customers
 *    parameters:
 *      - name: customer
 *        in: query
 *        description: Name of our customer
 *        required: false
 *        schema:
 *          type: string
 *          format: string
 *    responses:
 *      '201':
 *        description: Successfully created user
 */
router.put("/", (req, res) => {
  res.status(200).send("Successfully updated customer");
});

module.exports = router;
