const express = require("express");
const router = express.Router();

const customers = require("./customer");
const users = require("./user");

/**
 * Calling routes of Customers
 */
router.use("/customers", customers);

/**
 * Calling routes of Users
 */
router.use("/users", users);

module.exports = router;
