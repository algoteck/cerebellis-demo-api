# Introduction: cerebellis-demo-api

Cerebellis demo API for the purpose of Job application task. API is developed in reference to requirements in terms of project creation and use of modules, npm, OpenAPI, Testing and approach to full the task on daily basis.

# Project Deliverables

## Following are the objectives of the project

Checklist below shows the implementation of project objectives.

- #### Project Setup
  - #### [x] Installing dev, live and testing modules packages.
  - #### [x] Testing basic API end points.
  - #### [x] MongoDB Atlas Setup for the project.
  - #### [x] AWS Setup with CI/CD and testing endpoints online.
  - #### [x] Designing API endpoints.

# Project setup

## Installation Instrucitons:

In the root dir of the project run following command to install the installation files

```bash
#To download the project modules and packages run the following command in root dir.
npm i
```

# OpenAPI:

To open the OpenAPI specification documentation for this project. Click on the link below.

```bash
#OpenAPI Documentation
http://localhost:8000/api-docs
```

# Loading Database Schema:

MongoDB will be used for the purpose of this project.
As the project is aligned with MongoDB I will be using
MongoDB atlas for this purpose.

```bash
#For Live Application
CEREBELLIS-LIVE
- Live database instance will be used to run testcases of applicaiton.

#For Developement Application
CEREBELLIS-DEVELOPMENT
  - Developement database instance will be used to run testcases of applicaiton.

#For TEST Application
CEREBELLIS-TEST
  - Test database instance will be used to run testcases of applicaiton.
```

# Usage Instrucitons:

Following are the useful links for using Cerebellis Demo API

```bash
#Server Running on LIVE AWS with HTTP LINK
http://cerebellisnode-env.eba-fyr3bnnp.ap-southeast-2.elasticbeanstalk.com

#Server Running on localhost with HTTP LINK
http://localhost:8000/
```

# API Routes:

API Root Path

```bash
#For Localhost:
http://localhost:8000/

#For Live
http://cerebellisnode-env.eba-fyr3bnnp.ap-southeast-2.elasticbeanstalk.com
```

API Documentation

```bash
  API Description : /
     API Versions : /api
API Documentation : /api-docs
      API V1 URL  : /api/v1/
```

USER

```bash
GET users list  : /users
GET single user : /:id
     POST user  : /users
   UPDATE user  : /:id
   DELETE user  : /:id
```

CUSTOMER

```bash
GET customers list  : /
   UPDATE customer  : /:id
```

# Testing:

To run the test cases for the 'cerebellis-demo-api' run the following command in the root directory.

```bash
#open terminal in root dir
npm test
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
