const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const connectDB = require("./config/db");
const bodyParser = require("body-parser");

require("dotenv").config();

//Connect to Database
connectDB();

const app = express();

app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Main API Routes
 */
app.get("/", (req, res) => {
  res.json({
    message: "Cerebellis Demo Api ",
    description:
      "Cerebellis demo api for the description purposes of job application. Current file set it public and it is for the purpose of explaining the coding experience in terms of skill set and project procedures.",
  });
});

app.get("/api", (req, res) => {
  res.json({
    message: "Lists all versions of API",
    description: {
      version: "1.0.0",
      designedBy: "Muhammad Junaid Iftikhar",
      active: "true",
    },
  });
});

/**
 * OpenAPI Implementation
 */
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "1.0.0",
      title: "Ceremobills API",
      description:
        "Cerebellis demo api for the description purposes of job application. Current file set it public and it is for the purpose of explaining the coding experience in terms of skill set and project procedures.",
      contact: {
        name: "Muhammad Junaid Iftikhar",
      },
      host: "http://localhost:8000/api/v1",
      servers: ["http://localhost:8000/api/v1"],
    },
  },
  // ['.routes/*.js']
  apis: ["./routes/*.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
// serve swagger
app.get("/swagger.json", function (req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerDocs);
});
app.use("/api-docs", swaggerUi.serve);

/**
 * Adding API Routes
 */
const api_routes = require("./routes/index");
app.use("/api/v1", api_routes);

/**
 * Applicaiton Port configuration
 */
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

module.exports = app;
