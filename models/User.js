const mongoose = require("mongoose");
const { isEmail } = require("validator");

const userSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: [true, "Please enter first name"],
    unique: false,
    lowercase: true,
  },
  lastname: {
    type: String,
    required: [true, "Please enter last name"],
    unique: false,
    lowercase: true,
  },
  email: {
    type: String,
    required: [true, "Please enter an email"],
    unique: true,
    lowercase: true,
    validate: [isEmail, "Please enter a valid email"],
  },
  password: {
    type: String,
    required: [true, "Please enter a password"],
    minlength: [6, "Minimum password length is 6 characters"],
  },
});

// fire a function after doc saved to db
userSchema.post("save", function (doc, next) {
  console.log("new user was created & saved", doc);
  next();
});

// fire a function before doc saved to db
userSchema.pre("save", function (next) {
  console.log("//TODO :user about to be created {hash password} ", this);
  next();
});

const User = mongoose.model("user", userSchema);

module.exports = User;
