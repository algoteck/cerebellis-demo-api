//During the test the env variable is set to test
process.env.NODE_ENV = "TEST";

let mongoose = require("mongoose");
let USER = require("../models/User");

//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let app = require("../app");
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe("USER", () => {
  let user = {
    firstname: "Testing",
    lastname: "Data",
    email: "testing-data@gmail.com",
    password: "123456",
  };

  beforeEach((done) => {
    //Before each test we empty the database
    USER.deleteMany({}, (err) => {
      done();
    });
  });

  /**
   * POSITIVE TESTING
   */

  /*
   * Test the /GET route on /api/v1/users
   * Test Case # 1: it should GET all the users where list of users should be Zero.
   */
  describe("/GET /api/v1/users", () => {
    it("Test Case # 1: it should GET all the users where list of users should be Zero.", (done) => {
      chai
        .request(app)
        .get("/api/v1/users")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("success").eql(true);
          res.body.should.have.property("message");
          res.body.should.have.property("users").length(0);
          done();
        });
    });
  });

  /*
   * Test the /POST route on /api/v1/users
   * Test Case # 2: it should POST user data where list of users should be 1.
   */
  describe("/POST /api/v1/users", () => {
    it("Test Case # 2: it should POST user data where list of users should be 1.", (done) => {
      chai
        .request(app)
        .post("/api/v1/users")
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("success").eql(true);
          res.body.should.have.property("message");
          res.body.should.have.property("users").length(1);
          done();
        });
    });
  });

  /*
   * Test the /DELETE route on /api/v1/users/:id
   * Test Case # 3: it should DELETE user data where id is matching
   */
  describe("/DELETE /api/v1/users/:id", () => {
    it("Test Case # 3: it should DELETE user data where id is matching", (done) => {
      const userData = new USER(user);
      userData.save((err, user) => {
        chai
          .request(app)
          .delete("/api/v1/users/" + userData._id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("User is deleted with id " + userData._id);
            res.body.should.have.property("success").eql(true);
            done();
          });
      });
    });
  });

  /*
   * Test the /PUT route on /api/v1/users/:id
   * Test Case # 4: it should UPDATE user data where id is matching
   */
  describe("/PUT /api/v1/users/:id", () => {
    it("Test Case # 4: it should UPDATE user data where id is matching", (done) => {
      const userData = new USER(user);
      userData.save((err, user) => {
        chai
          .request(app)
          .put("/api/v1/users/" + userData._id)
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("User is updated with id " + userData._id);
            res.body.should.have.property("success").eql(true);
            done();
          });
      });
    });
  });
});
